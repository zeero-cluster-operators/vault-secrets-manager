FROM quay.io/operator-framework/ansible-operator:v1.4.2

USER root

RUN dnf install -y jq openssl unzip

RUN curl -LO https://releases.hashicorp.com/vault/1.6.3/vault_1.6.3_linux_amd64.zip && \
    unzip vault_1.6.3_linux_amd64.zip && \
    install vault /usr/local/bin/ && rm vault

COPY requirements.txt ${HOME}/requirements.txt
RUN pip3 install -r ${HOME}/requirements.txt

USER ansible

COPY requirements.yml ${HOME}/requirements.yml
RUN ansible-galaxy collection install -r ${HOME}/requirements.yml \
 && chmod -R ug+rwx ${HOME}/.ansible

COPY watches.yaml ${HOME}/watches.yaml
COPY roles/ ${HOME}/roles/
COPY playbooks/ ${HOME}/playbooks/
